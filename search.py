import re
from mechanize import Browser
from bs4 import BeautifulSoup
import click
import sys

class searchCouncil(object):

    """Docstring for searchCouncil. """

    def __init__(self, search_type='city'):
        """

        Constructor: 
            initiate the starting url for search
            prompt user to choose the search_type, default is 'city'

        """

        self.web_form = {'council': '3', 'city': '2'}
        self.search_type = search_type

        # This is the search form url
        self.url = "http://local.direct.gov.uk/LDGRedirect/index.jsp?LGSL=1465"
        self.urlmap = "http://local.direct.gov.uk/LDGRedirect/MapLocationSearch.do?LGSL=1465&LGIL=8&Style="
    
    def search_map(self):
        """
        Returns the URL of the council page 
        
        """
        # Initiate a browser object to navigate through the webpage
        br = Browser()
        # Avoid reading the robots.txt file
        br.set_handle_robots(False)
        br.open(self.urlmap)
        html = br.response().read()
        soup = BeautifulSoup(html, 'html.parser')
        # Now search the HTML for the rowContainer class
        ul_tag = soup.find_all('ul', attrs={'class': 'resultsList'})[0]
        options = {}
        links=[]
        i = 0
        for item in ul_tag.find_all('a'):
            # print item.text, item['href']
            links.append(item['href'])
            options[str(i)] = [item.text, br.geturl() + item['href']]
            i += 1

        return options
        

    def search_data(self, CouncilName='Southampton'):
        """

        Returns the URL of the council page which has the
        raw data

        """
    
        # Initiate a browser object to navigate through the webpage
        br = Browser()
        # Avoid reading the robots.txt file
        br.set_handle_robots(False)
        br.open(self.url)

        # For debugging:
        # print br.title()
        # Search through the forms of the webpage and print accordingly
        # for f in br.forms():
        #     print "Form name: ", f.name
        #     print f

        # print "*******************************"

        # the HTML form name from the url (found with the previous code)
        br.select_form('locationform')
        # Select according to what was specified as search type
        br.form['searchtype'] = [self.web_form[self.search_type]]
        br.form['text'] = CouncilName
        # Send the information written to the form
        br.submit()

        # Get the HTML code from the curent page
        html = br.response().read()
        soup = BeautifulSoup(html, 'html.parser')
        # Now search the HTML for the rowContainer class
        div_tag = soup.find_all('div', attrs={'class': 'rowContainer'})
        # Check if the result have the specific heading below:
        if (len(div_tag) == 0): 

            # Make a BS object to read through the code easily
            page = BeautifulSoup(html, 'html.parser')
            print 'Results:'

            # We will save the urls
            links = []

            # Get all the ul tags with an specific class. Since there is only
            # one of these ul's, we extract the only element from
            # the findall results (thats the [0])
            # This is a TAG object from BS
            ul_tag = page.find_all('ul', attrs={'class': 'resultsList'})[0]
            # We will get all the href links (a) from the ul tag object
            # using findAll and then
            # iterate through them to extract the 'href' values

            # Options is a dictionary where every entry is a list
            # containing the option name and the URL and the keys
            # are a sequence of integers: 1,2,3...
            options = {}
            i = 0
            for item in ul_tag.find_all('a'):
                # print item.text, item['href']
                links.append(item['href'])
                options[str(i)] = [item.text, br.geturl() + item['href']]
                i += 1

            return options

        else:
            return {'0': [CouncilName, br.geturl()]}

    def process_link(self, url):
        """
            Return a link object of a council page
            from the URL specified in the argument
        """
        print 'Searching page link with CSV files ***************************'

        # This URL is the direct.gov page with 'Search results' heading
        # (chosen from a list or from a unique link in after the
        # search engine)
        br = Browser()
        br.set_handle_robots(False)
        br.open(url)

        # Now we get the whole HTML code with response() and search through
        # it using BeautifulSoup
        html = br.response().read()
        soup = BeautifulSoup(html, 'html.parser')
        # Now search a HTML in the rowContainer class
        div_tag = soup.find_all('div', attrs={'class': 'rowContainer'})[0]

        # Find all the links in the previous HTML class
        item = div_tag.find_all('a')[0]

        # Now we are going to follow links that have the 500 pounds string
        # in the name until find a page without it (that we assume will
        # be the page with the CSV files)
        search_link = item['href']
        while True:
            # Open a URL from the BS results
            # print 'Following: ', search_link
            br.open(search_link)
            # Read the URL
            html = br.response().read()
            soup = BeautifulSoup(html, 'html.parser')
            # Get all the possible links with 500 pounds
            url_list = soup.find_all('a', text=re.compile('\\xa3500'))

            # Is there is no link with 500 pounds, just return the
            # current URL
            if len(url_list) == 0:
                return search_link
            # Otherwise, check if the links found are not CSV files
            # and keep searching returning to the BS opening
            else:
                # First try to search a CSV file from thee whole list
                # with 500 pounds in name
                for url in url_list:
                    if url['href'].endswith('.csv'):
                        print 'Found CSV link!', url['href']
                        return search_link

                # Otherwise, try to follow the links. Before doing that,
                # check if the url to follow is pointing to itself
                if re.search(url_list[0]['href'], search_link):
                    return search_link

                search_link = url_list[0]['href']

    def abort_if_false(ctx, param, value):
        if not value:
            ctx.abort()

    # @click.command()
    # @click.option('--yes', is_flag=True,
    #               callback=abort_if_false,
    #               expose_value=False,
    #               prompt='Are you sure you want to download the url CSV files?')
    def download_data(self, url):
        """

        Download data from the specified url

        """

        br = Browser()
        br.set_handle_robots(False)
        br.open(url)
        html = br.response().read()

        # creates a page object which has all the info of the html page
        page = BeautifulSoup(html, 'html.parser')

        # finds the link objects (a) in the html page where the href identifier
        # end with .csv
        for a in page.find_all('a', href=re.compile(r'.*\.csv$')):
            print "Found the URL:", a['href']
            # downloads the file in the link given as the first argument
            # and saves it into filename given in the second argument
            print a.text
            # try:
            br.retrieve(a['href'],
                            a.text.replace(' ', '_') + '.csv')
            #except:
            #    print 'Could not find links with CSV files'

# Execute if the class is called as the main program
if __name__ == '__main__':
    # Name = "Southampton"
    # mySearch = searchCouncil()
    # mySearch.search_data()
    """
    # Show a prompt asking for search type using click
    value = click.prompt('Choose a search type (city/council)')
    if value in ['city', 'council']:
        mySearch = searchCouncil(search_type=value)
    else:
        print 'Invalid option'
        sys.exit(0)

    # This is the text in the webpage form to search a council
    value = click.prompt('Type a city or council')

    options = mySearch.search_data(CouncilName=value)

    # Sort the 'options' dictionary keys converting
    # the sorted entries into integers (since they are strings)
    # For doing this, we use a one line function (lambda)
    for _key in sorted(options.keys(), key=lambda i: int(i)):
        print _key, options[_key][0]

    options_number = len(options.keys())
    print '{} All'.format(options_number)

    value = click.prompt('Choose option number')
    # fallow the links of all the councils listed
    # if the user chooses the all option 
    # otherwise check the council the user gave the input for 
    # and fallow the link of that
    if int(value) == options_number:
        for i in range(options_number):
            myurl = mySearch.process_link(options[str(i)][1])
            print myurl

    elif int(value) in range(options_number):
        myurl = mySearch.process_link(options[value][1])
        print myurl, '\n'
        # mySearch.download_data(myurl)

    # elif value == str(options_number):
    #     pass
    # else:
    #     print 'Introduce a valid number'
    #     sys.exit(0)
    """
    mySearch = searchCouncil()
    options=mySearch.search_map()
    for _key in sorted(options.keys(), key=lambda i: int(i)):
        print _key, options[_key][0]    